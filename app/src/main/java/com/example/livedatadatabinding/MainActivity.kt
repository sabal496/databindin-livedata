package com.example.livedatadatabinding

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log.d
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.example.livedatadatabinding.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        //setContentView(R.layout.activity_main)
        val model=ViewModelProvider(this)[MainViewModel::class.java]
        val binding:ActivityMainBinding=DataBindingUtil.setContentView(this,R.layout.activity_main)
        binding.viewmodel=model
        binding.lifecycleOwner=this
        model.email.observe(this, Observer {
            d("email",it)
        })
    }
}
