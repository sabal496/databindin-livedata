package com.example.livedatadatabinding

import android.os.Handler
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel



class MainViewModel:ViewModel(){
    val  BarVisible=MutableLiveData<Boolean>()
    val Result=MutableLiveData<String>()
    val Textcolor=MutableLiveData<Boolean>()
    val email=MutableLiveData<String>()
    fun lohIn(email:String,passwor:String){
        BarVisible.value=true
        Handler().postDelayed({
            BarVisible.value=false
            if(email.isEmpty() || passwor.isEmpty()) {
                Result.value="failure all fields are requared"
                Textcolor.value=false
            }
            else{
                Textcolor.value=true
                Result.value="logged in suscsessfully"
            }
        },5000)
    }

}