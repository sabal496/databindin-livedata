package com.example.livedatadatabinding
import android.view.View
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.databinding.BindingAdapter

@BindingAdapter("visibility")
fun View.setVisibility(visible:Boolean){
    visibility=if(visible) View.VISIBLE
    else
        View.GONE

}

@BindingAdapter("textcolor")
fun TextView.setcolor(color: Boolean){
    if(color) setTextColor(ContextCompat.getColor(App.instance,R.color.green))
    else setTextColor(ContextCompat.getColor(App.instance,R.color.red))
}